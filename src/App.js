import React, { Component } from 'react';
import Todos from "./components/Todos";
import AddTodos from "./components/AddTodos";


class App extends Component {

  state = {
    todos: [
        {id: 1, content: "Go home"},
        {id: 2, content: "Coding"},
        {id: 3, content: "Drink coffee"},
    ]
  }


  deleteTodo = (id) => {
    let todos = this.state.todos.filter(todo => {
      return todo.id !== id;
    })
      this.setState({
          todos: todos
      })
  }

  addTodo = (todo) => {
    todo.id = Math.random();
    console.log(todo);
    let todos = [...this.state.todos, todo];
    this.setState({
        todos: todos
    })
  }

  render() {
    return (
      <div className="todo-app container">
        <h2 className="center blue-text">Todo List</h2>
        <Todos todos={this.state.todos} deleteTodo={this.deleteTodo}/>
        <AddTodos addTodo={this.addTodo} />
      </div>
    );
  }
}

export default App;
