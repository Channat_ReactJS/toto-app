import React, {Component} from 'react'


class AddTodos extends Component {

    state = {
        content: null,
    }

    handleOnChange = (e) => {
        this.setState({
            content: e.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.addTodo(this.state);
        this.setState({
            content: ''
        })
    }


    render() {
        return (
            <div className="todo row">
                <form onSubmit={this.handleSubmit}>
                    <div className="input-field">
                        <input id="content" type="text" placeholder="What will you do?" onChange={this.handleOnChange} value={this.state.content}/>
                    </div>
                </form>
            </div>
        )
    }

}


export default AddTodos